/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef APPMANAGER_H
#define APPMANAGER_H

#include <QObject>
#include <QVector>
#include "common.h"
#include "core/appinfomanager.h"
#include "dbusservices/whitelistmanager.h"

class AppManagerService;
class AppLauncher;
class WhiteListManager;
class MemoryMonitor;
class EventWatcher;
class AppStatusManager;
class AppManager : public QObject
{
    Q_OBJECT
public:
    explicit AppManager(QObject *parent = nullptr);

    Q_INVOKABLE bool LaunchApp(const QString &desktopFile);
    Q_INVOKABLE bool LaunchAppWithArguments(const QString &desktopFile,const QStringList &args);
    Q_INVOKABLE bool Open(const QString &fileName);
    Q_INVOKABLE bool LaunchDefaultAppWithUrl(const QString &url);
    Q_INVOKABLE void SetPowerSavingModeEnable(bool isPowerSaving);
    Q_INVOKABLE bool AddToWhiteList(const QString &desktopFile, const QString &option);
    Q_INVOKABLE bool RemoveFromWhiteList(const QString &desktopFile, const QString &option);
    Q_INVOKABLE bool ActiveProcessByWid(const uint &wid);
    Q_INVOKABLE bool ActiveProcessByPid(const int &pid);

    Q_INVOKABLE QStringList WhiteListsOfApp(const QString &desktopFile);
    Q_INVOKABLE QStringList AppWhiteList(const QString &option);
    Q_INVOKABLE QVector<QStringList> RecommendAppLists(const QString &fileName);
    Q_INVOKABLE QString AppDesktopFileNameByPid(qint64 pid);
    Q_INVOKABLE QString AppDesktopFileNameByWid(qint64 wid);
    Q_INVOKABLE QString Inhibit(const QString &desktopFile,
                                uint pid,
                                const QString &reason,
                                uint flags);
    Q_INVOKABLE void UnInhibit(const QString &cookie);

    Q_INVOKABLE bool ThawApps();

private:
    void initConnections();

private:
    AppManagerService *m_service;
    AppLauncher *m_appLauncher;
    EventWatcher *m_eventWatcher;
    WhiteListManager &m_whiteListManager = common::Singleton<WhiteListManager>::GetInstance();
    AppInfoManager &m_appInfoManager = common::Singleton<AppInfoManager>::GetInstance();
};

#endif // APPMANAGER_H
