/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MISC_H
#define MISC_H

#include <QObject>
#include <QJsonObject>

namespace utils {

class Misc : public QObject
{
    Q_OBJECT
public:
    explicit Misc(QObject *parent = nullptr);
    static bool ensurePathExists(const QString &path);
    static bool ensureFileExists(const QString &fileName);
    static QJsonObject readJsonFromFile(const QString &fileName);
    static bool writeJsonToFile(const QJsonObject &obj, const QString &fileName);
};

} // namespace utils


#endif // MISC_H
