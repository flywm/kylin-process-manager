/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "appmanagerservice.h"
#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

/*
 * Implementation of adaptor class AppManagerService
 */

AppManagerService::AppManagerService(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    // constructor
    setAutoRelaySignals(true);
}

AppManagerService::~AppManagerService()
{
    // destructor
}

bool AppManagerService::ActiveProcessByPid(int pid)
{
    // handle method call com.kylin.AppManager.ActiveProcessByPid
    bool succeed;
    QMetaObject::invokeMethod(parent(), "ActiveProcessByPid", Q_RETURN_ARG(bool, succeed), Q_ARG(int, pid));
    return succeed;
}

bool AppManagerService::ActiveProcessByWid(uint wid)
{
    // handle method call com.kylin.AppManager.ActiveProcessByWid
    bool succeed;
    QMetaObject::invokeMethod(parent(), "ActiveProcessByWid", Q_RETURN_ARG(bool, succeed), Q_ARG(uint, wid));
    return succeed;
}

bool AppManagerService::AddToWhiteList(const QString &desktopFile, const QString &option)
{
    // handle method call com.kylin.AppManager.AddToWhiteList
    bool succeed;
    QMetaObject::invokeMethod(parent(), "AddToWhiteList", Q_RETURN_ARG(bool, succeed), Q_ARG(QString, desktopFile), Q_ARG(QString, option));
    return succeed;
}

QString AppManagerService::AppDesktopFileNameByPid(qlonglong pid)
{
    // handle method call com.kylin.AppManager.AppDesktopFileNameByPid
    QString desktopFileName;
    QMetaObject::invokeMethod(parent(), "AppDesktopFileNameByPid", Q_RETURN_ARG(QString, desktopFileName), Q_ARG(qlonglong, pid));
    return desktopFileName;
}

QString AppManagerService::AppDesktopFileNameByWid(qlonglong wid)
{
    // handle method call com.kylin.AppManager.AppDesktopFileNameByWid
    QString desktopFileName;
    QMetaObject::invokeMethod(parent(), "AppDesktopFileNameByWid", Q_RETURN_ARG(QString, desktopFileName), Q_ARG(qlonglong, wid));
    return desktopFileName;
}

QStringList AppManagerService::AppWhiteList(const QString &option)
{
    // handle method call com.kylin.AppManager.AppWhiteList
    QStringList appLists;
    QMetaObject::invokeMethod(parent(), "AppWhiteList", Q_RETURN_ARG(QStringList, appLists), Q_ARG(QString, option));
    return appLists;
}

QString AppManagerService::Inhibit(const QString &desktopFile, uint pid, const QString &reason, uint flags)
{
    // handle method call com.kylin.AppManager.Inhibit
    QString cookie;
    QMetaObject::invokeMethod(parent(), "Inhibit", Q_RETURN_ARG(QString, cookie), Q_ARG(QString, desktopFile), Q_ARG(uint, pid), Q_ARG(QString, reason), Q_ARG(uint, flags));
    return cookie;
}

bool AppManagerService::LaunchApp(const QString &desktopFile)
{
    // handle method call com.kylin.AppManager.LaunchApp
    bool succeed;
    QMetaObject::invokeMethod(parent(), "LaunchApp", Q_RETURN_ARG(bool, succeed), Q_ARG(QString, desktopFile));
    return succeed;
}

bool AppManagerService::LaunchAppWithArguments(const QString &desktopFile, const QStringList &args)
{
    // handle method call com.kylin.AppManager.LaunchAppWithArguments
    bool succeed;
    QMetaObject::invokeMethod(parent(), "LaunchAppWithArguments", Q_RETURN_ARG(bool, succeed), Q_ARG(QString, desktopFile), Q_ARG(QStringList, args));
    return succeed;
}

bool AppManagerService::LaunchDefaultAppWithUrl(const QString &url)
{
    // handle method call com.kylin.AppManager.LaunchDefaultAppWithUrl
    bool succeed;
    QMetaObject::invokeMethod(parent(), "LaunchDefaultAppWithUrl", Q_RETURN_ARG(bool, succeed), Q_ARG(QString, url));
    return succeed;
}

bool AppManagerService::Open(const QString &fileName)
{
    // handle method call com.kylin.AppManager.Open
    bool succeed;
    QMetaObject::invokeMethod(parent(), "Open", Q_RETURN_ARG(bool, succeed), Q_ARG(QString, fileName));
    return succeed;
}

QVector <QStringList> AppManagerService::RecommendAppLists(const QString &fileName)
{
    // handle method call com.kylin.AppManager.RecommendAppLists
    QVector <QStringList> appLists;
    QMetaObject::invokeMethod(parent(), "RecommendAppLists", Q_RETURN_ARG(QVector <QStringList>, appLists), Q_ARG(QString, fileName));
    return appLists;
}

bool AppManagerService::RemoveFromWhiteList(const QString &desktopFile, const QString &option)
{
    // handle method call com.kylin.AppManager.RemoveFromWhiteList
    bool succeed;
    QMetaObject::invokeMethod(parent(), "RemoveFromWhiteList", Q_RETURN_ARG(bool, succeed), Q_ARG(QString, desktopFile), Q_ARG(QString, option));
    return succeed;
}

void AppManagerService::SetPowerSavingModeEnable(bool enable)
{
    // handle method call com.kylin.AppManager.SetPowerSavingModeEnable
    QMetaObject::invokeMethod(parent(), "SetPowerSavingModeEnable", Q_ARG(bool, enable));
}

bool AppManagerService::ThawApps()
{
    // handle method call com.kylin.AppManager.ThawApps
    bool succeed;
    QMetaObject::invokeMethod(parent(), "ThawApps", Q_RETURN_ARG(bool, succeed));
    return succeed;
}

void AppManagerService::UnInhibit(const QString &cookie)
{
    // handle method call com.kylin.AppManager.UnInhibit
    QMetaObject::invokeMethod(parent(), "UnInhibit", Q_ARG(QString, cookie));
}

QStringList AppManagerService::WhiteListsOfApp(const QString &desktopFile)
{
    // handle method call com.kylin.AppManager.WhiteListsOfApp
    QStringList whiteLists;
    QMetaObject::invokeMethod(parent(), "WhiteListsOfApp", Q_RETURN_ARG(QStringList, whiteLists), Q_ARG(QString, desktopFile));
    return whiteLists;
}

