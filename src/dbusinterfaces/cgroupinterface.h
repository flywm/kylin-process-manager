/*
 * Copyright (C) 2020 The Qt Company Ltd.
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CGROUPINTERFACE_H
#define CGROUPINTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

/*
 * Proxy class for interface com.kylin.CGroup.AppManager
 */
class CGroupInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "com.kylin.CGroup.AppManager"; }

public:
    CGroupInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = nullptr);

    ~CGroupInterface();

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<> CreateCGroup()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("CreateCGroup"), argumentList);
    }

    inline QDBusPendingReply<> DeleteCGroup()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("DeleteCGroup"), argumentList);
    }

    inline QDBusPendingReply<> MoveProcess(int pid, int group)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(pid) << QVariant::fromValue(group);
        return asyncCallWithArgumentList(QStringLiteral("MoveProcess"), argumentList);
    }

Q_SIGNALS: // SIGNALS
};

namespace com {
  namespace kylin {
    namespace CGroup {
      typedef ::CGroupInterface AppManager;
    }
  }
}
#endif
