QT += widgets dbus KConfigCore KConfigGui KWindowSystem concurrent KWaylandClient

CONFIG += c++11 console link_pkgconfig no_keywords
CONFIG -= app_bundle

CONFIG += c++11 link_pkgconfig

PKGCONFIG += kysdk-waylandhelper

DBUS_INTERFACES += conf/com.kylin.ProcessManager.xml

PKGCONFIG += glib-2.0 gio-2.0 gio-unix-2.0
include(QtSingleApplication/qtsingleapplication.pri)
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS #INVISIBLE_ENABLE

INCLUDEPATH += \
     src

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/core/appcgroup.cpp \
    src/core/appinfo.cpp \
    src/core/appinfomanager.cpp \
    src/core/appstatusmanager.cpp \
    src/core/basecontroller.cpp \
    src/core/eventwatcher.cpp \
    src/core/policy.cpp \
    src/core/timerwheel.cpp \
    src/dbusinterfaces/statusmanagerinterface.cpp \
    src/main.cpp \
    src/appmanager.cpp \
    src/dbusinterfaces/cgroupinterface.cpp \
    src/dbusinterfaces/kwininterface.cpp \
    src/dbusservices/applauncher.cpp \
    src/dbusservices/applauncherdaemon.cpp \
    src/dbusservices/appmanagerservice.cpp \
    src/dbusservices/whitelistmanager.cpp \
    src/memorymonitor.cpp \
    src/utils/misc.cpp \
    src/utils/processinfo.cpp

# Default rules for deployment.
CONFIG(debug, debug|release) {
    target.path = /home/kylin
} else {
    target.path = /usr/bin
}
#unit.files = $$PWD/debian/lib/systemd/user/kylin-app-manager.service
#unit.path = /lib/systemd/user
#service.files = $$PWD/debian/usr/share/dbus-1/services/com.kylin.AppManager.service
#service.path = /usr/share/dbus-1/services/
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/appmanager.h \
    src/core/appcgroup.h \
    src/core/appinfo.h \
    src/core/appinfomanager.h \
    src/core/appstatusmanager.h \
    src/core/basecontroller.h \
    src/core/eventwatcher.h \
    src/core/policy.h \
    src/core/timerwheel.h \
    src/dbusinterfaces/cgroupinterface.h \
    src/dbusinterfaces/kwininterface.h \
    src/dbusinterfaces/statusmanagerinterface.h \
    src/dbusservices/applauncher.h \
    src/dbusservices/applauncherdaemon.h \
    src/dbusservices/appmanagerservice.h \
    src/dbusservices/whitelistmanager.h \
    src/memorymonitor.h \
    src/utils/misc.h \
    src/utils/processinfo.h \
    src/common.h

LIBS += -lukui-log4qt
